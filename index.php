<?php
// Bentuk fungsi
require ('animal.php');
require ('ape.php');
require ('frog.php');

$sheep = new Animal("shaun");
echo "Nama Domba :" . $sheep->name . "<br>"; // "shaun"
echo "Berkaki :" . $sheep->legs . "<br>"; // 2
echo "Berdarah Dingin :";
var_dump($sheep->cold_blooded); // false

echo "<br><br>";
$sungokong = new Ape ("kera sakti");
echo "Nama Kera :" . $sungokong->name . "<br>";
echo "Berkaki :" . $sungokong->legs . "<br>";
echo "Berteriak :";
var_dump($sungokong->yell()); // Auooo;
echo "<br><br>";

$kodok = new Frog ("buduk");
echo "Nama Kodok :" . $kodok->name . "<br>";
echo "Berkaki :" . $kodok->legs . "<br>";
echo "Melompat  :";
var_dump($kodok->jump()); // "hop hop"

?>